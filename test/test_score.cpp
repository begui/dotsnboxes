#include <gtest/gtest.h>
#include <algorithm>
#include "board.hpp"

constexpr auto boardSize{4};

int getScore ( const std::vector< char >& gamestate, const int xRow, const int yCol, const int x, const int y );

TEST ( LOC_0_0, DoesRtnZERO ) {
  std::vector< char > gamestate ( boardSize * boardSize );
  std::fill ( gamestate.begin ( ), gamestate.end ( ), NoVal );
  int xRow{Board::SIZE};
  int yCol{Board::SIZE};
  int x{0};
  int y{0};

  EXPECT_EQ ( 0, getScore ( gamestate, xRow, yCol, x, y ) );

  SUCCEED ( );
}

TEST ( A, B ) { SUCCEED ( ); }

int main ( int argc, char** argv ) {
  ::testing::InitGoogleTest ( &argc, argv );
  return RUN_ALL_TESTS ( );
}
