# Dots N Boxes


## Overview
 This is an implementation of the [Dots N Boxes](https://en.wikipedia.org/wiki/Dots_and_Boxes) using the [Minimax](https://en.wikipedia.org/wiki/Minimax) 


### Current status
Incomplete 


### Resources
- [Dots N Boxes](https://en.wikipedia.org/wiki/Dots_and_Boxes)
- [Minimax](https://en.wikipedia.org/wiki/Minimax) 
- [Minimax-alphabeta](http://web.cs.ucla.edu/~rosen/161/notes/alphabeta.html)


## Building

#### Linux 
```
mkdir build && cd build
cmake ../ 
make
```
#### Mac 
```
TODO
```
#### Windows
```
TODO
```
#### Emscripten

 * Read the "getting started" page to set up SDK 

```
# Fetch the latest registry of available tools
./emsdk update

# Download and install the latest SDK tools.
./emsdk install latest

# Make the "latest" SDK "active"
./emsdk activate latest

```

 * For SDL2 follow the following

  [SDL2 Emscriten](https://hg.libsdl.org/SDL/file/tip/docs/README-emscripten.md)

```
emconfigure ./configure --disable-assembly --disable-threads --enable-cpuinfo=false --enable-joystick=no CFLAGS="-O2"
```

 * Make sure you have sourced in your Emscripten Env Variables and run the following 

```
source ./emsdk_env.sh
mkdir build && cd build
cmake ../ -DUSE_EMSCRIPTEN=1 
make
```
