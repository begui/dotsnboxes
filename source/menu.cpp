#include "menu.hpp"
#include <iostream>
#include <limits>
#include "game.hpp"

constexpr int hDivider{40};

Menu::Selection& operator++ ( Menu::Selection& selection ) {
  using IntType = typename std::underlying_type< Menu::Selection >::type;
  selection = static_cast< Menu::Selection > ( static_cast< IntType > ( selection ) + 1 );
  if ( selection == Menu::Selection::END ) {
    selection = static_cast< Menu::Selection > ( 1 );
  }
  return selection;
}

Menu::Selection operator++ ( Menu::Selection& selection, int ) {
  Menu::Selection rtnSelection = selection;
  ++selection;
  return rtnSelection;
}

Menu::Selection& operator-- ( Menu::Selection& selection ) {
  using IntType = typename std::underlying_type< Menu::Selection >::type;
  selection = static_cast< Menu::Selection > ( static_cast< IntType > ( selection ) - 1 );
  if ( selection == Menu::Selection::BEGIN ) {
    selection = static_cast< Menu::Selection > ( static_cast< IntType > ( Menu::Selection::END ) - 1 );
  }
  return selection;
}

Menu::Selection operator-- ( Menu::Selection& selection, int ) {
  Menu::Selection rtnSelection = selection;
  --selection;
  return rtnSelection;
}
void Menu::next ( ) { ++currentMenuItem_; }
void Menu::prev ( ) { --currentMenuItem_; }

Menu::Menu ( ) {
  int i = 0;
  auto setFont = [&]( auto selection, auto text ) {
    menuFont[ i ].backgroundFontTexture = sdl2::surface_to_texture< sdl2::TextureShrdPtr > (
        //
        Game::renderer ( ),
        //
        sdl2::render_ttf< sdl2::SurfaceUniqPtr > ( Game::font ( ), sdl2::TTFRenderMode::Blended, text,
                                                   {255, 255, 255, 255} )
        //
        );
    menuFont[ i ].foregroundFontTexture = sdl2::surface_to_texture< sdl2::TextureShrdPtr > (
        Game::renderer ( ), sdl2::render_ttf< sdl2::SurfaceUniqPtr > ( Game::font ( ), sdl2::TTFRenderMode::Blended,
                                                                       text, {255, 0, 255, 255} ) );
    menuFont[ i ].menuItem = selection;
    ++i;

  };
  menuFont[ i ].backgroundFontTexture = nullptr;
  menuFont[ i ].foregroundFontTexture = nullptr;
  menuFont[ i ].menuItem = Selection::BEGIN;
  ++i;

  setFont ( Selection::PLAYER_ONE, "1 Player" );
  setFont ( Selection::PLAYER_TWO, "2 Player" );
  setFont ( Selection::QUIT, "Quit" );

  menuFont[ i ].backgroundFontTexture = nullptr;
  menuFont[ i ].foregroundFontTexture = nullptr;
  menuFont[ i ].menuItem = Selection::END;
}

void Menu::render ( ) {
  const auto windowSize = sdl2::get_logical_size ( Game::renderer ( ) );
  auto w{std::get< 0 > ( windowSize )};
  auto h{std::get< 1 > ( windowSize )};
  int h2{0};
  static std::int32_t rate{8};

  for ( int i = Selection::QUIT; i >= Selection::PLAYER_ONE; --i ) {
    sdl2::render_texture_at ( Game::renderer ( ),
                              //
                              menuFont[ i ].backgroundFontTexture.get ( ),
                              //
                              nullptr,
                              //
                              w / 2 - 32,
                              //
                              h / 2 - h2 );

    if ( currentMenuItem_ == menuFont[ i ].menuItem ) {
      // TODO: this is currently bound to fps... lets hope vsync always works
      std::uint8_t alpha = sdl2::get_texture_alpha ( menuFont[ i ].foregroundFontTexture.get ( ) );
      static const auto min = std::numeric_limits< std::uint8_t >::min ( );
      static const auto max = std::numeric_limits< std::uint8_t >::max ( );

      if ( static_cast< int > ( rate + alpha ) <= static_cast< int > ( min ) ) {
        alpha = min;
        rate *= -1;
      } else if ( static_cast< int > ( rate + alpha ) >= static_cast< int > ( max ) ) {
        alpha = max;
        rate *= -1;
      } else {
        alpha += rate;
      }

      sdl2::set_texture_alpha ( menuFont[ i ].foregroundFontTexture.get ( ), alpha );

      sdl2::render_texture_at ( Game::renderer ( ),
                                //
                                menuFont[ i ].foregroundFontTexture.get ( ),
                                //
                                nullptr,
                                //
                                w / 2 - 32,
                                //
                                h / 2 - h2 );
    }
    h2 += hDivider;
  }
}

// void Menu::update ( double delta ) {}
