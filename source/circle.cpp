#include "circle.hpp"
#ifndef __EMSCRIPTEN__
#include <SDL2/SDL2_gfxPrimitives.h>
#endif
#include "game.hpp"

void Circle::draw ( ) const {

#ifndef __EMSCRIPTEN__
  filledCircleRGBA ( Game::renderer ( ), x_, y_, radius_, color_.r, color_.g, color_.b, color_.a );
  // seems to contain a bug, reverting to RGBA
  // filledCircleColor ( Game::renderer ( ), x_, y_, radius_, color_ );
#else
  SDL_SetRenderDrawColor ( Game::renderer(), color_.r, color_.g, color_.b, color_.a );
  SDL_Rect rec{x_-10,y_-10,20,20};
  SDL_RenderFillRect ( Game::renderer(), &rec); 
#endif
}

bool Circle::isPointInCircle ( std::int32_t x, std::int32_t y ) const {
  // distance formula
  std::int32_t answer = static_cast< int > (
      //
      sqrt (
          //
          ( ( x - x_ ) * ( x - x_ ) )
          //
          +
          //
          ( ( y - y_ ) * ( y - y_ ) )
          //
          ) );

  return answer <= radius_;
}
void Circle::setColor ( const SDL_Color &color ) { color_ = color; }
SDL_Color Circle::getColor ( ) const { return color_; }

// void Circle::setColor ( std::uint32_t color ) { color_ = color; }
// std::uint32_t Circle::getColor ( ) const { return color_; }
void Circle::setX ( std::int32_t x ) { x_ = x; }
void Circle::setY ( std::int32_t y ) { y_ = y; }
std::int32_t Circle::getX ( ) const { return x_; }
std::int32_t Circle::getY ( ) const { return y_; }
