#include "game.hpp"
#include <iostream>
#include "board.hpp"
#include "minimax.hpp"
#ifdef __EMSCRIPTEN__
#include<emscripten.h>
#endif

SDL_Window *Game::window_;
SDL_Renderer *Game::renderer_;
TTF_Font *Game::font_;

SDL_Window *Game::window ( ) { return Game::window_; }
SDL_Renderer *Game::renderer ( ) { return Game::renderer_; }
TTF_Font *Game::font ( ) { return Game::font_; }

void Game::setWindow ( SDL_Window *window ) { Game::window_ = window; }
void Game::setRenderer ( SDL_Renderer *renderer ) { Game::renderer_ = renderer; }
void Game::setFont ( TTF_Font *font ) { Game::font_ = font; }


void event_loop ( Game *game ) {

  static Menu menu;

  sdl2::clear ( Game::renderer ( ) );
  //
  // Input handler
  //
  SDL_Event event;
  while ( SDL_PollEvent ( &event ) ) {
    if ( event.type == SDL_QUIT ) {
      game->running_ = false;
    }

    if ( game->isMenuVisable_ ) {
      if ( event.type == SDL_KEYDOWN ) {
        if ( 0 == event.key.repeat ) {
          switch ( event.key.keysym.sym ) {
            case SDLK_UP:
              menu.prev ( );
              break;
            case SDLK_DOWN:
              menu.next ( );
              break;
            case SDLK_RETURN:
              if ( menu.getCurrentMenuItem ( ) == Menu::Selection::QUIT ) {
                game->running_ = false;
              } else {
                auto numPlayers = static_cast< int > ( menu.getCurrentMenuItem ( ) );
                if ( numPlayers == Menu::Selection::PLAYER_ONE ) {
                  game->player[ 1 ].isAi = true;
                }
                game->isMenuVisable_ = false;
              }
              break;
            case SDLK_ESCAPE:
              game->running_ = false;
              break;
            default:
              break;
          }
        }
      }
    } else {
      // Not in Menu
      if ( event.type == SDL_KEYDOWN ) {
        if ( 0 == event.key.repeat ) {
          switch ( event.key.keysym.sym ) {
            case SDLK_ESCAPE:
              game->isMenuVisable_ = true;
              break;
            default:
              break;
          }
        }
      }
      if ( event.type == SDL_MOUSEBUTTONDOWN || event.type == SDL_FINGERDOWN ) {
        if ( !game->player[ game->playerToggle_ ].isAi ) {  // if it's players twos turn and is not the AI

          std::int32_t x{0};
          std::int32_t y{0};
          if ( event.type == SDL_MOUSEBUTTONDOWN ) {
            x = event.button.x;
            y = event.button.y;
          }
          if ( event.type == SDL_FINGERDOWN ) {
            x = event.tfinger.x;
            y = event.tfinger.y;
          }
          game->moveMade_ = game->board_.makeMove ( game->player[ game->playerToggle_ ], x, y );
        }
      }
    }
  }  // end of while
  //
  // Compute the AI's turn
  //
  if ( game->player[ game->playerToggle_ ].isAi ) {
    game->board_.makeAiMove ( game->player[ game->playerToggle_ ] );
    game->moveMade_ = true;
  }
  //
  // increment game counter and change player
  //
  if ( game->moveMade_ ) {
    game->playerToggle_ = !game->playerToggle_;
    game->gameCounter_++;
    game->moveMade_ = false;
  }
  //
  // Rendering
  //
  if ( game->isMenuVisable_ ) {
    menu.render ( );
  } else {

    //
    // Render Grid
    //
    static sdl2::TextureUniqPtr grid{nullptr};
    if ( !grid ) {
      const auto cellWidth{100};
      const auto cellHeight{100};
      const auto width{game->width_};
      const auto height{game->height_};
      const SDL_Color color{128, 128, 128, 64};

      auto drawGrid = std::bind ( [height, width, &cellWidth, &cellHeight, &color] {
        SDL_SetRenderDrawColor ( Game::renderer ( ), color.r, color.g, color.b, color.a );

        for ( int x = 0; x < width; x += cellWidth ) {
          SDL_RenderDrawLine ( Game::renderer ( ), x, 0, x, height - 1 );
        }
        for ( int y = 0; y < height; y += cellHeight ) {
          SDL_RenderDrawLine ( Game::renderer ( ), 0, y, width - 1, y );
        }

        for ( int x = 0; x < width; x += cellWidth ) {
          for ( int y = 0; y < height; y += cellHeight ) {
            SDL_RenderDrawLine ( Game::renderer ( ), x, y, x + cellWidth, y + cellHeight );
            SDL_RenderDrawLine ( Game::renderer ( ), x + cellWidth, y, x, y + cellHeight );
          }
        }
      } );
      grid = sdl2::render_to_texture_uniq ( drawGrid, Game::renderer ( ), width, height );
    }
    sdl2::render_texture_at ( Game::renderer ( ), grid.get ( ), nullptr, 0, 0 );
    //
    // Renders the board
    //
    auto renderLine = [&]( int row, int column, int rowOffset, int columnOffset ) {
      if ( game->board_[ {row, column} ].isSet ( ) && game->board_[ {row + rowOffset, column + columnOffset} ].isSet ( ) ) {
        SDL_RenderDrawLine ( Game::renderer ( ),
                             //
                             game->board_[ {row, column} ].circle ( ).getX ( ),
                             //
                             game->board_[ {row, column} ].circle ( ).getY ( ),
                             //
                             game->board_[ {row + rowOffset, column + columnOffset} ].circle ( ).getX ( ),
                             //
                             game->board_[ {row + rowOffset, column + columnOffset} ].circle ( ).getY ( ) );
      }
    };

    for ( std::size_t i = 0; i < Board::SIZE; ++i ) {
      for ( std::size_t j = 0; j < Board::SIZE; ++j ) {
        SDL_SetRenderDrawColor ( Game::renderer ( ), 255, 255, 255, 255 );
        if ( i == Board::SIZE - 1 || j == Board::SIZE - 1 ) {
          if ( i == j && i == Board::SIZE - 1 ) {
            renderLine ( i, j, 0, -1 );
            renderLine ( i, j, -1, 0 );
          } else if ( i == Board::SIZE - 1 && j != 0 ) {
            renderLine ( i, j, 0, -1 );
          } else if ( j == Board::SIZE - 1 && i != 0 ) {
            renderLine ( i, j, -1, 0 );
          }

        } else {
          renderLine ( i, j, 0, 1 );
          renderLine ( i, j, 1, 0 );
        }
      }
    }
    //
    // render balls
    //
    for ( std::size_t i = 0; i < Board::SIZE; ++i ) {
      for ( std::size_t j = 0; j < Board::SIZE; ++j ) {
        game->board_[ {i, j} ].circle ( ).draw ( );
      }
    }
    //
    // Renders the score board
    //
    auto renderScore = []( auto &player, auto text, auto color, int width, int height, int widthOffset,
                           int heightOffset ) {
      player.playerString = sdl2::surface_to_texture< sdl2::TextureShrdPtr > (
          //
          Game::renderer ( ),
          //
          sdl2::render_ttf< sdl2::SurfaceUniqPtr > ( Game::font ( ), sdl2::TTFRenderMode::Blended, text, color )
          //
          );

      int x = width / 2 + widthOffset;
      int y = height + heightOffset;
      sdl2::render_texture_at ( Game::renderer ( ), player.playerString.get ( ), nullptr, x, y );
    };

    renderScore ( game->player[ 0 ], std::string ( "P1:" + std::to_string ( game->player[ 0 ].score ) ).c_str ( ),
                  SDL_Color{0, 0, 255, 255}, game->width_, game->height_, -100, -50 );
    renderScore ( game->player[ 1 ], std::string ( "P2:" + std::to_string ( game->player[ 1 ].score ) ).c_str ( ),
                  SDL_Color{255, 0, 0, 255}, game->width_, game->height_, 100, -50 );
  }
  //
  // Swap the window
  //
  sdl2::swap ( Game::window ( ) );
  //
  // Check if game is over
  //
  if ( game->gameCounter_ == Board::SIZE * Board::SIZE ) {
    game->running_ = false;
  }

  if ( game->player[ 0 ].score > game->player[ 1 ].score ) {
    // Player 1 won
  } else {
    // Player 2 won
  }
}

void event_loop_emscripten ( Game *game ) {

  static Menu menu;

  sdl2::clear ( Game::renderer ( ), {255, 0, 0, 255} );
  //
  // Input handler
  //
  SDL_Event event;
  while ( SDL_PollEvent ( &event ) ) {
    if ( event.type == SDL_QUIT ) {
      game->running_ = false;
    }
  }
}

void Game::run ( ) {

  //
  // Init board
  //
  const auto windowSize = sdl2::get_logical_size ( Game::renderer ( ) );
  width_ = std::get< 0 > ( windowSize );
  height_ = std::get< 1 > ( windowSize );

  board_.init ( width_, height_ );

#ifdef __EMSCRIPTEN__
  emscripten_set_main_loop_arg ( ( em_arg_callback_func ) event_loop, this, 0, 1 );
#else
  while ( running_ ) {
    event_loop ( this );
  }
#endif
}
