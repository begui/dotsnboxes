#include "board.hpp"
#include <algorithm>
#include <functional>
#include <iterator>

int getScore ( const std::vector< char >& gamestate, const int xRow, const int yCol, const int x, const int y ) {
  // auto getIndex = [&]( int x, int y ) { return x + y * xRow; };
  auto getIndex = [&]( int x, int y ) { return y + x * yCol; };
  auto isSet = [&]( int xoffset, int yoffset ) {
    const int offsetIndex = getIndex ( xoffset, yoffset );
    return gamestate[ offsetIndex ] != NoVal;
  };
  // 1 2 3
  // 4 5 6
  // 7 8 9
  int score{0};
  if ( x == 0 && y == 0 ) {
    // from 1 => 5, 4, 2
    if ( isSet ( x + 1, y + 1 ) && isSet ( x + 1, y ) && isSet ( x, y + 1 ) ) {
      score += 1;
    }
  } else if ( x == xRow - 1 && y == yCol - 1 ) {
    // from 9 => 6, 5, 8
    if ( isSet ( x - 1, y ) && isSet ( x - 1, y - 1 ) && isSet ( x, y - 1 ) ) {
      score += 1;
    }
  } else {
    // from 5 => 9,8,6
    if ( x != xRow - 1 && y != yCol - 1 && isSet ( x + 1, y + 1 ) && isSet ( x + 1, y ) && isSet ( x, y + 1 ) ) {
      score += 1;
    }
    // from 5 => 2,3,6
    if ( x != 0 && y != yCol - 1 && isSet ( x - 1, y ) && isSet ( x - 1, y + 1 ) && isSet ( x, y + 1 ) ) {
      score += 1;
    }
    // from 5 => 2, 1, 4
    if ( x != 0 && y != 0 && isSet ( x - 1, y ) && isSet ( x - 1, y - 1 ) && isSet ( x, y - 1 ) ) {
      score += 1;
    }
    // from 5 => 4, 7, 8
    if ( x != xRow - 1 && y != 0 && isSet ( x, y - 1 ) && isSet ( x + 1, y - 1 ) && isSet ( x + 1, y ) ) {
      score += 1;
    }
  }
  return score;
}

std::vector< char > flattenArray ( const Board* board ) {
  std::vector< char > flattenArr;
  std::transform ( &board->cells_[ 0 ][ 0 ], &board->cells_[ 0 ][ 0 ] + Board::SIZE * Board::SIZE,
                   std::back_inserter ( flattenArr ), []( const Board::BoardCell& cell ) {
                     if ( cell.isSet ( ) ) {
                       return ( cell.getPlayer ( )->isAi ) ? 'O' : 'X';
                     } else {
                       return '_';
                     }
                   } );
  return flattenArr;
}

void Board::init ( int width, int height ) {
  int startX = width / 2 - ( int ) ( Board::spacing * Board::offset );
  int startY = height / 2 - ( int ) ( Board::spacing * Board::offset );

  for ( std::size_t i = 0; i < Board::SIZE; ++i ) {
    for ( std::size_t j = 0; j < Board::SIZE; ++j ) {
      auto x = startX + j * Board::spacing;
      auto y = startY + i * Board::spacing;
      cells_[ i ][ j ].circle ( ).setX ( x );
      cells_[ i ][ j ].circle ( ).setY ( y );
    }
  }
}

bool Board::makeMove ( Player& player, int x, int y ) {
  for ( std::size_t i = 0; i < Board::SIZE; ++i ) {
    for ( std::size_t j = 0; j < Board::SIZE; ++j ) {
      if ( !cells_[ i ][ j ].isSet ( ) && cells_[ i ][ j ].circle ( ).isPointInCircle ( x, y ) ) {
        cells_[ i ][ j ].circle ( ).setColor ( player.color );
        cells_[ i ][ j ].setPlayer ( &player );

        auto flattenArr = flattenArray ( this );
        player.score += getScore ( flattenArr, Board::SIZE, Board::SIZE, i, j );

        print ( i, j );
        return true;
      }
    }
  }
  return false;
}

void Board::makeAiMove ( Player& player ) {
  using namespace std::placeholders;

  int positionX{0}, positionY{0};
  auto flattenArr = flattenArray ( this );
  minimax_.run ( flattenArr, Board::SIZE, Board::SIZE, getScore, positionX, positionY );

  if ( cells_[ positionX ][ positionY ].isSet ( ) ) {
    std::cerr << "This position [" << positionX << "][" << positionY
              << "] is already taken. Something is wrong with the minimax tree" << std::endl;

    return;
  }
  cells_[ positionX ][ positionY ].circle ( ).setColor ( player.color );
  cells_[ positionX ][ positionY ].setPlayer ( &player );

  print ( positionX, positionY );

  //
  // Issue where I am not inserting into the correct location, just going to recalculate it by calling the
  // the flattenArray function. 
  //
  /*  
  //flattenArr.insert ( flattenArr.begin ( ) + ( positionX + Board::SIZE * positionY ), P2orAI );
  //flattenArr.insert ( flattenArr.begin ( ) + ( positionY + positionX *  Board::SIZE ), P2orAI );
  */
  flattenArr = flattenArray(this);
  player.score += getScore ( flattenArr, Board::SIZE, Board::SIZE, positionX, positionY );
}

void Board::print ( int x, int y ) const {
  std::cout << x << " " << y << std::endl;
  for ( std::size_t i = 0; i < Board::SIZE; ++i ) {
    for ( std::size_t j = 0; j < Board::SIZE; ++j ) {
      if ( cells_[ i ][ j ].isSet ( ) ) {
        std::cout << cells_[ i ][ j ].getPlayer ( )->name << " ";
      } else {
        std::cout << "__ ";
      }
    }
    std::cout << std::endl;
  }
  std::cout << std::endl;

  auto flattenArr = flattenArray ( this );
  int i{0};
  for ( const auto& vec : flattenArr ) {
    std::cout << vec << " ";
    ++i;
    if ( i % Board::SIZE == 0 ) {
      std::cout << std::endl;
    }
  }
}
