#!/bin/bash

if [ ! -d "build/" ]; then
	echo "Please run the following " 
	echo "mkdir build && cd build && cmake ../ && make" 
	exit 1
fi

clang-tidy -p build/ -checks="*" source/*.cpp -- -std=c++14 -Iinclude/ -Iinclude/rgd -I/usr/include/

