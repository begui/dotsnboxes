#pragma once
#include <cstdint>
#include "rgd/RGD_sdl2.hpp"

namespace color {
/*
constexpr std::uint32_t RED{0xFF0000FF};
constexpr std::uint32_t BLUE{0x0000FFFF};
constexpr std::uint32_t WHITE{0xFFFFFFFF};
*/
constexpr SDL_Color RED{255, 0, 0, 255};
constexpr SDL_Color BLUE{0, 0, 255, 255};
constexpr SDL_Color WHITE{255, 255, 255, 255};
}

class Circle {
 public:
  Circle ( ) = default;
  Circle ( std::int32_t x, std::int32_t y ) : x_{x}, y_{y} {}

 public:
  void draw ( ) const;
  bool isPointInCircle ( std::int32_t x, std::int32_t y ) const;
  // void setColor ( std::uint32_t color );
  // std::uint32_t getColor ( ) const;
  void setColor ( const SDL_Color &color );
  SDL_Color getColor ( ) const;
  void setX ( std::int32_t x );
  void setY ( std::int32_t y );
  std::int32_t getX ( ) const;
  std::int32_t getY ( ) const;

 private:
  std::int32_t x_;
  std::int32_t y_;
  // std::uint32_t color_{color::WHITE};
  SDL_Color color_{color::WHITE};
  std::int32_t radius_{12};
};
