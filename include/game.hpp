#pragma once

#include "board.hpp"
#include "menu.hpp"
#include "rgd/RGD_sdl2.hpp"

class Game {
 public:
  void run ( );

 public:
  static SDL_Window *window ( );
  static SDL_Renderer *renderer ( );
  static TTF_Font *font ( );
  static void setWindow ( SDL_Window *window );
  static void setRenderer ( SDL_Renderer *renderer );
  static void setFont ( TTF_Font *font );

 private:
  static SDL_Window *window_;
  static SDL_Renderer *renderer_;
  static TTF_Font *font_;

 private:

  friend void event_loop(Game *game);
  friend void event_loop_emscripten(Game *game);

 private:
  Board board_;
  Player player[ 2 ] {{"P1", color::BLUE}, {"P2", color::RED}};
  bool playerToggle_{false};
  bool isMenuVisable_{true};  // TODO: clean this up and build gamestate
  bool moveMade_{false};
  bool running_{true};
  std::int32_t gameCounter_{0};
  std::int32_t width_;
  std::int32_t height_;
};
