#pragma once
#include <algorithm>
#include <array>
#include <functional>
#include <iostream>
#include <limits>
#include <memory>
#include <random>
#include <vector>

/**
 * MiniMax with the following techniques for two players only
 *
 *  - Negamax
 *  - Nega Scout
 *  - Standard Minimax
 *  - Alpha-Beta Pruning .
 */
constexpr static auto P1{'X'};
constexpr static auto P2orAI{'O'};
constexpr static auto NoVal{'_'};
constexpr static auto MIN{std::numeric_limits< std::int32_t >::min ( )};
constexpr static auto MAX{std::numeric_limits< std::int32_t >::max ( )};

template < typename T, std::size_t BRANCH_SIZE >
class MiniMax {
 public:
  struct node {
    std::int32_t selectedIndex{0};
    std::int32_t cost{0};
    std::int32_t alpha{MIN};  // maximum lower bound
    std::int32_t beta{MAX};   // minimum upper bound
    std::vector< T > boardState;
    std::array< std::shared_ptr< node >, BRANCH_SIZE > nodes;
  };

 public:
  MiniMax ( std::int32_t treeDepth = 1 ) : treeDepth_{treeDepth} {}

 private:
  std::int32_t treeDepth_{4};
  bool shuffle_{true};

 public:
  bool getShuffle ( ) const { return shuffle_; }
  void setShuffle ( bool shuffle ) { shuffle_ = shuffle; }

 public:
  template < typename COST_FUNC >
  void run ( std::vector< T > &boardState, const std::size_t XROW, const std::size_t YCOL, COST_FUNC &costFun,
             int &xpos, int &ypos ) {
    auto head = std::make_shared< node > ( );
    // copy the current state of the game
    head->boardState = boardState;

    minimax ( P2orAI, head, XROW, YCOL, costFun, treeDepth_ );

    // check that head->nodes is not null
    head->cost = 0;
    head->selectedIndex = 0;
    for ( std::size_t i = 0; i < head->nodes.size ( ) && head->nodes[ i ]; i++ ) {
      if ( head->nodes[ i ]->cost >= head->cost ) {
        head->cost = head->nodes[ i ]->cost;
        head->selectedIndex = head->nodes[ i ]->selectedIndex;
      }
    }
    //
    // We need to find the minimum
    //
    xpos = head->selectedIndex / XROW;
    ypos = head->selectedIndex % XROW;
  }

 private:
  /**
   * Computes the entire tree till depth, ( defaults to 4 ) and determines if the correct path based on
   * the cost of all nodes.
   */
  template < typename COST_FUNC >
  void minimax ( char player, std::shared_ptr< node > N, const std::size_t XROW, const std::size_t YCOL,
                 COST_FUNC &costFun, int depth ) {
    /**
     * We want to get a list of all positions in the flatten array that are available for the AI to choose from.
     * @return a list of positions the AI will use to calculate the minimax tree
     */
    auto findEmptyLocation = []( const std::vector< T > &boardState, bool bShuffle ) {

      std::vector< std::size_t > availableState;
      // get a list of indexes
      for ( std::size_t i = 0; i < boardState.size ( ); ++i ) {
        if ( NoVal == boardState[ i ] ) {
          availableState.push_back ( i );
        }
      }
      availableState.shrink_to_fit ( );
      if ( bShuffle ) {
        static std::random_device rd;
        static std::mt19937 gen ( rd ( ) );
        std::shuffle ( availableState.begin ( ), availableState.end ( ), gen );
      }
      return availableState;
    };

    //
    // create the nodes for N
    //
    if ( depth == 0 ) {
      int selectedIndex = N->selectedIndex;
      int xrow = selectedIndex / XROW;
      int ycol = selectedIndex % XROW;
      // if we reached the end of our depth or at the end of the game, compute the cost
      N->cost = costFun ( N->boardState, XROW, YCOL, xrow, ycol );
    } else {
      auto emptyLocations = findEmptyLocation ( N->boardState, shuffle_ );
      if ( !emptyLocations.empty ( ) ) {
        for ( std::size_t i = 0; i < BRANCH_SIZE && i < emptyLocations.size ( ); i++ ) {
          int selectedIndex = emptyLocations[ i ];
          // TODO: could probably remove the pointer
          N->nodes[ i ] = std::make_shared< struct node > ( );
          N->nodes[ i ]->cost = ( player == P1 ) ? MIN : MAX;
          N->nodes[ i ]->boardState = N->boardState;
          N->nodes[ i ]->selectedIndex = selectedIndex;
          N->nodes[ i ]->boardState[ selectedIndex ] = player;
          // call recursively
          minimax ( player == P1 ? P2orAI : P1, N->nodes[ i ], XROW, YCOL, costFun, depth - 1 );
        } 

        std::int32_t bestcost =  ( player == P1 ) ? MIN : MAX;
        for ( std::size_t i = 0; i < N->nodes.size ( ) && N->nodes[ i ]; i++ ) {
          if ( player == P1 && N->nodes[ i ]->cost > bestcost ) {
            bestcost = N->nodes[i]->cost;
          }
          if ( player == P2orAI && N->nodes[ i ]->cost < bestcost ) {
            bestcost = N->nodes[i]->cost;
          }
        }
        N->cost = bestcost;
      }
    }
  }
  /**
   *
   *
  template < typename COST_FUNC >
  void negamax ( char player, std::shared_ptr< node > N, const std::size_t XROW, const std::size_t YCOL,
                 COST_FUNC &costFun, int depth ) {
    // TODO: Implement 
  }
  */

  /**
   * Instead of computing all nodes, this will find a good match and go that route, eliminating the entire
   * tree from being generated.
  template < typename COST_FUNC >
  void alphabeta ( char player, std::shared_ptr< node > N, const std::size_t XROW, const std::size_t YCOL,
                   COST_FUNC &costFun, int depth ) {
    // TODO: Implement 
  }
   */

  // TODO: look into negascout algorithms
};
