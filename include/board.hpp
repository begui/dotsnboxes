#pragma once
#include <iostream>
#include "circle.hpp"
#include "minimax.hpp"

struct Player {
  Player ( const std::string& name, const SDL_Color& color ) : name{name}, color{color} {}
  std::string name;
  SDL_Color color;
  bool isAi{false};
  int score{0};
  sdl2::TextureShrdPtr playerString{nullptr};
};

class Board final {
 public:
   constexpr static auto spacing{50};

   /*  
   constexpr static auto SIZE{10};
   constexpr static auto offset{4.5};

   */
   constexpr static auto SIZE{5};
   constexpr static auto offset{1.5};

 public:
  class BoardCell final {
   public:
    Circle& circle ( ) { return circle_; }
    bool isSet ( ) const { return player_ != nullptr; }
    void setPlayer ( Player* player ) { player_ = player; }
    Player* getPlayer ( ) const { return player_; }

   private:
    Circle circle_;
    Player* player_{nullptr};
  };

 public:
  BoardCell& operator[] ( const std::pair< int, int >& Index ) { return cells_[ Index.first ][ Index.second ]; }

 public:
  /**
   *
   */
  void init ( int width, int height );
  /**
   *
   */
  bool makeMove ( Player& player, int x, int y );
  /**
   * The method tries to determine the move the AI player will make.
   */
  void makeAiMove ( Player& player );
  /**
   *
   */
  void print ( int x, int y ) const;

 private:
  /**
   *
   */
  friend std::vector< char > flattenArray ( const Board *board) ;

 private:
  MiniMax< char, Board::SIZE * Board::SIZE> minimax_;
  std::array< std::array< BoardCell, Board::SIZE >, Board::SIZE > cells_;
};
