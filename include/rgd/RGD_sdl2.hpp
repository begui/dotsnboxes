#pragma once

#include <memory>
#include <string>
#include <type_traits>
#include <vector>

#include <SDL2/SDL.h>
#include <SDL2/SDL_syswm.h>
#ifdef USE_SDL2_TTF
#ifdef __EMSCRIPTEN__
#include <SDL_ttf.h>
#else
#include <SDL2/SDL_ttf.h>
#endif
#endif
#ifdef USE_SDL2_MIXER
#include <SDL2/SDL_mixer.h>
#endif
#ifdef USE_SDL2_IMAGE
#include <SDL2/SDL_image.h>
#endif

#ifdef USE_GL
#include "RGD_gl.hpp"
#endif

// TODO: we may not need USE_GL when using sdl, these methods should always
// exist when using sdl

namespace sdl2 {

namespace internal {
template < typename T >
constexpr auto to_type ( T t ) {
  return static_cast< typename std::underlying_type< T >::type > ( t );
}
}

enum class SubSystemFlags : std::int8_t {
  //
  CORE = 0x00,
  //
  TTF = 0x01,
  //
  IMAGE = 0x02,
  //
  AUDIO = 0x04
};

enum class BLENDMODE : std::int32_t {
  // None:
  // dstRGBA = srcRGBA
  NONE = SDL_BLENDMODE_NONE,
  // Alpha:
  // dstRGB = (srcRGB * srcA) + (dstRGB * (1-srcA))
  // dstA = srcA + (dstA * (1-srcA))
  BLEND = SDL_BLENDMODE_BLEND,
  // Additive blending:
  // dstRGB = (srcRGB * srcA) + dstRGB
  // dstA = dstA
  ADD = SDL_BLENDMODE_ADD,
  // Color modulate:
  // dstRGB = srcRGB * dstRGB
  // dstA = dstA
  MOD = SDL_BLENDMODE_MOD
};

enum INIT : std::int32_t {
  //
  TIMER = SDL_INIT_TIMER,
  //
  AUDIO = SDL_INIT_AUDIO,
  //
  VIDEO = SDL_INIT_VIDEO,
  //
  JOYSTICK = SDL_INIT_JOYSTICK,
  //
  HAPTIC = SDL_INIT_HAPTIC,
  //
  CONTROLER = SDL_INIT_GAMECONTROLLER,
  //
  EVENTS = SDL_INIT_EVENTS,
  //
  EVERYTHING = SDL_INIT_EVERYTHING
};

enum WINDOW : std::int32_t {
  // full screen
  FULLSCREEN = SDL_WINDOW_FULLSCREEN,
  // full screen at current desktop rez
  FULLSCREEN_DESKTOP = SDL_WINDOW_FULLSCREEN_DESKTOP,
  // opengl window
  OPENGL = SDL_WINDOW_OPENGL,
  // window is not visable
  HIDDEN = SDL_WINDOW_HIDDEN,
  // no decorations
  BORDERLESS = SDL_WINDOW_BORDERLESS,
  // allows to be resizable
  RESIZEABLE = SDL_WINDOW_RESIZABLE,
  // window is minimized
  MINIMIZED = SDL_WINDOW_MINIMIZED,
  // window is maximized
  MAXIMIZED = SDL_WINDOW_MAXIMIZED,
  // window has grab input focus
  INPUTGRAB = SDL_WINDOW_INPUT_GRABBED,
  // high dpi if supported
  HIGHDPI = SDL_WINDOW_ALLOW_HIGHDPI
};

enum RENDERER : std::int32_t {
  // software acceleration
  SOFTWARE = SDL_RENDERER_SOFTWARE,
  // hardware acceleration
  ACCELERATED = SDL_RENDERER_ACCELERATED,
  // allows vsync
  VSYNC = SDL_RENDERER_PRESENTVSYNC,
  // supports rendering to a texture
  TEXTURE = SDL_RENDERER_TARGETTEXTURE
};

struct SDLDeleter {
  void operator( ) ( SDL_Window *ptr ) {
    if ( ptr ) SDL_DestroyWindow ( ptr );
  }
  void operator( ) ( SDL_Renderer *ptr ) {
    if ( ptr ) SDL_DestroyRenderer ( ptr );
  }
  void operator( ) ( SDL_Surface *ptr ) {
    if ( ptr ) SDL_FreeSurface ( ptr );
  }
  void operator( ) ( SDL_Texture *ptr ) {
    if ( ptr ) SDL_DestroyTexture ( ptr );
  }
#ifdef USE_GL
  void operator( ) ( SDL_GLContext *context ) {
    if ( context ) SDL_GL_DeleteContext ( *context );
  }
#endif
#ifdef USE_SDL2_TTF
  void operator( ) ( TTF_Font *ptr ) {
    if ( ptr ) TTF_CloseFont ( ptr );
  }
#endif
#ifdef USE_SDL2_MIXER
  void operator( ) ( Mix_Chunk *ptr ) {
    if ( ptr ) Mix_FreeChunk ( ptr );
  }
  void operator( ) ( Mix_Music *ptr ) {
    if ( ptr ) Mix_FreeMusic ( ptr );
  }
#endif
};

using WindowUniqPtr = std::unique_ptr< SDL_Window, SDLDeleter >;
using WindowShrdPtr = std::shared_ptr< SDL_Window >;
using WindowWeakPtr = std::weak_ptr< SDL_Window >;

using RendererUniqPtr = std::unique_ptr< SDL_Renderer, SDLDeleter >;
using RendererShrdPtr = std::shared_ptr< SDL_Renderer >;
using RendererWeakPtr = std::weak_ptr< SDL_Renderer >;

using SurfaceUniqPtr = std::unique_ptr< SDL_Surface, SDLDeleter >;
using SurfaceShrdPtr = std::shared_ptr< SDL_Surface >;
using SurfaceWeakPtr = std::weak_ptr< SDL_Surface >;

using TextureUniqPtr = std::unique_ptr< SDL_Texture, SDLDeleter >;
using TextureShrdPtr = std::shared_ptr< SDL_Texture >;
using TextureWeakPtr = std::weak_ptr< SDL_Texture >;

#ifdef USE_GL
using GLContextUniqPtr = std::unique_ptr< SDL_GLContext, SDLDeleter >;
#endif

#ifdef USE_SDL2_TTF
using TTFUniqPtr = std::unique_ptr< TTF_Font, SDLDeleter >;
using TTFShrdPtr = std::shared_ptr< TTF_Font >;
using TTFPWeakPtr = std::weak_ptr< TTF_Font >;
enum class TTFRenderMode : std::int8_t { Blended, Shaded, Solid };
#endif
#ifdef USE_SDL2_MIXER
using MixChunkUniqPtr = std::unique_ptr< Mix_Chunk, SDLDeleter >;
using MixChunkShrdPtr = std::shared_ptr< Mix_Chunk >;
using MixChunkWeakPtr = std::weak_ptr< Mix_Chunk >;

using MixMusicUniqPtr = std::unique_ptr< Mix_Music, SDLDeleter >;
using MixMusicShrdPtr = std::shared_ptr< Mix_Music >;
using MixMusicWeakPtr = std::weak_ptr< Mix_Music >;

#endif

class SDL2Exception : public std::runtime_error {
 public:
  explicit SDL2Exception ( const char *message )
      : std::runtime_error ( std::string ( message ) + " SDL_GetError() " + SDL_GetError ( ) ) {}
  explicit SDL2Exception ( const std::string &message )
      : std::runtime_error ( message + " SDL_GetError() " + SDL_GetError ( ) ) {}
  virtual ~SDL2Exception ( ) throw ( ) {}
};
//
// Function prototypes
//
void set_blend_mode ( SDL_Texture *texture, BLENDMODE mode );
BLENDMODE get_blend_mode ( SDL_Texture *texture );
void set_transparent_pixel ( SDL_Surface *surface, bool enable, int r, int g, int b );

/**
 * Initializes sdl, don't forget to call SDL_Quit
 */
inline void init ( std::int32_t flags = INIT::EVERYTHING ) {
  if ( SDL_Init ( flags ) != 0 ) {
    throw SDL2Exception ( "Failed to initialize sdl2." );
  }
}
#ifdef USE_SDL2_TTF
inline void init_ttf ( ) {
  if ( TTF_Init ( ) != 0 ) {
    throw SDL2Exception ( "Failed to initialize sdl2 ttf." );
  }
}
#endif
/**
 *
 **/
template < typename T >
T create_window ( std::uint32_t flags, int width, int height, int x = SDL_WINDOWPOS_UNDEFINED,
                  int y = SDL_WINDOWPOS_UNDEFINED );

template <>
inline SDL_Window *create_window ( std::uint32_t flags, int width, int height, int x, int y ) {
  return SDL_CreateWindow ( "", x, y, width, height, flags );
}
template <>
inline WindowUniqPtr create_window ( std::uint32_t flags, int width, int height, int x, int y ) {
  return WindowUniqPtr ( create_window< SDL_Window * > ( flags, width, height, x, y ) );
}

template <>
inline WindowShrdPtr create_window ( std::uint32_t flags, int width, int height, int x, int y ) {
  return WindowShrdPtr ( create_window< WindowUniqPtr > ( flags, width, height, x, y ) );
}
/**
 *
 **/
template < typename T >
T create_renderer ( SDL_Window *window, int flags );

template <>
inline SDL_Renderer *create_renderer ( SDL_Window *window, int flags ) {
  return SDL_CreateRenderer ( window, -1, flags );
}
template <>
inline RendererUniqPtr create_renderer ( SDL_Window *window, int flags ) {
  return RendererUniqPtr ( create_renderer< SDL_Renderer * > ( window, flags ) );
}
template <>
inline RendererShrdPtr create_renderer ( SDL_Window *window, int flags ) {
  return RendererShrdPtr ( create_renderer< RendererUniqPtr > ( window, flags ) );
}

inline bool isJoyStickEnabled ( ) { return SDL_WasInit ( SDL_INIT_JOYSTICK ) != 0 ? true : false; }
inline bool isHapticEnabled ( ) { return SDL_WasInit ( SDL_INIT_HAPTIC ) != 0 ? true : false; }
inline bool isGameControllerEnabled ( ) { return SDL_WasInit ( SDL_INIT_GAMECONTROLLER ) != 0 ? true : false; }
inline bool isEventsEnabled ( ) { return SDL_WasInit ( SDL_INIT_EVENTS ) != 0 ? true : false; }
inline bool isAudioEnabled ( ) { return SDL_WasInit ( SDL_INIT_AUDIO ) != 0 ? true : false; }
inline bool isTimerEnabled ( ) { return SDL_WasInit ( SDL_INIT_TIMER ) != 0 ? true : false; }
inline bool isVideoEnabled ( ) { return SDL_WasInit ( SDL_INIT_VIDEO ) != 0 ? true : false; }
#ifdef USE_SDL2_TTF
inline bool isTTFEnabled ( ) { return TTF_WasInit ( ) != 0 ? true : false; }
#endif

/*
 *
 */
template < typename PtrType, std::uint32_t SIZE >
inline std::array< PtrType, SIZE > create_renderers ( SDL_Window *window, int flags ) {
  std::array< PtrType, SIZE > renderers;
  for ( int i = 0; i < SIZE; ++i ) {
    renderers[ i ] = sdl2::create_renderer< PtrType > ( window, flags );
  }
  return renderers;
}

/**
 *
 */
template < typename T >
T create_surface ( int width, int height, int depth = 32 );

template <>
inline SDL_Surface *create_surface ( int width, int height, int depth ) {
#if SDL_BYTEORDER == SDL_BIG_ENDIAN
  auto rmask = 0xff000000;
  auto gmask = 0x00ff0000;
  auto bmask = 0x0000ff00;
  auto amask = 0x000000ff;
#else
  auto rmask = 0x000000ff;
  auto gmask = 0x0000ff00;
  auto bmask = 0x00ff0000;
  auto amask = 0xff000000;
#endif

  auto ptr = SDL_CreateRGBSurface ( 0, width, height, depth, rmask, gmask, bmask, amask );
  if ( !ptr ) {
    throw SDL2Exception ( "Failed to create surface ." );
  }
  return ptr;
}
template <>
inline SurfaceUniqPtr create_surface ( int width, int height, int depth ) {
  return SurfaceUniqPtr ( create_surface< SDL_Surface * > ( width, height, depth ) );
}

template <>
inline SurfaceShrdPtr create_surface ( int width, int height, int depth ) {
  return SurfaceShrdPtr ( create_surface< SurfaceUniqPtr > ( width, height, depth ) );
}

/**
 *
 */
template < typename T >
T create_surface ( int width, int height, int depth, const SDL_Color &color );

template <>
inline SDL_Surface *create_surface ( int width, int height, int depth, const SDL_Color &color ) {
  auto ptr = create_surface< SDL_Surface * > ( width, height, depth );
  set_transparent_pixel ( ptr, ptr->format, color.r, color.g, color.b );
  return ptr;
}

template <>
inline SurfaceUniqPtr create_surface ( int width, int height, int depth, const SDL_Color &color ) {
  return SurfaceUniqPtr ( create_surface< SDL_Surface * > ( width, height, depth, color ) );
}

template <>
inline SurfaceShrdPtr create_surface ( int width, int height, int depth, const SDL_Color &color ) {
  return SurfaceShrdPtr ( create_surface< SurfaceUniqPtr > ( width, height, depth, color ) );
}
/**
 *
 */
template < typename T >
T create_surface ( const char *filename, const SDL_Color &color = {255, 0, 255, 255} );

template <>
inline SDL_Surface *create_surface ( const char *filename, const SDL_Color &color ) {
#if USE_SDL2_IMAGE
  auto ptr = IMG_Load ( filename );
#else
  auto ptr = SDL_LoadBMP ( filename );
#endif
  if ( !ptr ) {
    throw SDL2Exception ( "Failed to create surface ." );
  }
  set_transparent_pixel ( ptr, ptr->format, color.r, color.g, color.b );
  return ptr;
}
template <>
inline SurfaceUniqPtr create_surface ( const char *filename, const SDL_Color &color ) {
  return SurfaceUniqPtr ( create_surface< SDL_Surface * > ( filename, color ) );
}
template <>
inline SurfaceShrdPtr create_surface ( const char *filename, const SDL_Color &color ) {
  return SurfaceShrdPtr ( create_surface< SurfaceUniqPtr > ( filename, color ) );
}

/**
 *
 **/
template < typename T >
T surface_to_texture ( SDL_Renderer *renderer, SurfaceUniqPtr &&surface );

template <>
inline SDL_Texture *surface_to_texture ( SDL_Renderer *renderer, SurfaceUniqPtr &&surface ) {
  return SDL_CreateTextureFromSurface ( renderer, surface.get ( ) );
}
template <>
inline TextureUniqPtr surface_to_texture ( SDL_Renderer *renderer, SurfaceUniqPtr &&surface ) {
  return TextureUniqPtr (
      surface_to_texture< SDL_Texture * > ( renderer, std::forward< SurfaceUniqPtr > ( surface ) ) );
}
template <>
inline TextureShrdPtr surface_to_texture ( SDL_Renderer *renderer, SurfaceUniqPtr &&surface ) {
  return TextureShrdPtr (
      surface_to_texture< TextureUniqPtr > ( renderer, std::forward< SurfaceUniqPtr > ( surface ) ) );
}
/**
 *
 **/
template < typename T >
T create_texture ( SDL_Renderer *rendderer, std::int32_t pixelFormat, int textureAccess, int width, int height,
                   bool enableBlendMode );

template <>
inline SDL_Texture *create_texture ( SDL_Renderer *renderer, int pixelFormat, int textureAccess, int width, int height,
                                     bool enableBlendMode ) {
  auto ptr = SDL_CreateTexture ( renderer, pixelFormat, textureAccess, width, height );
  if ( !ptr ) {
    throw SDL2Exception ( "Failed to create texture." );
  }
  if ( enableBlendMode ) {
    sdl2::set_blend_mode ( ptr, BLENDMODE::BLEND );
  }
  return ptr;
}

template <>
inline TextureUniqPtr create_texture ( SDL_Renderer *renderer, int pixelFormat, int textureAccess, int width,
                                       int height, bool enableBlendMode ) {
  return TextureUniqPtr (
      create_texture< SDL_Texture * > ( renderer, pixelFormat, textureAccess, width, height, enableBlendMode ) );
}

template <>
inline TextureShrdPtr create_texture ( SDL_Renderer *renderer, int pixelFormat, int textureAccess, int width,
                                       int height, bool enableBlendMode ) {
  return TextureShrdPtr (
      create_texture< TextureUniqPtr > ( renderer, pixelFormat, textureAccess, width, height, enableBlendMode ) );
}
/**
 *
 **/
template < typename T >
T create_texture ( SDL_Renderer *renderer, const char *filename, const SDL_Color &color );

template <>
inline SDL_Texture *create_texture ( SDL_Renderer *renderer, const char *filename, const SDL_Color &color ) {
  auto surfacePtr = create_surface< SurfaceUniqPtr > ( filename, color );
  if ( !surfacePtr ) {
    throw SDL2Exception ( "Failed to create Texture." );
  }
  return surface_to_texture< SDL_Texture * > ( renderer, std::move ( surfacePtr ) );
}

template <>
inline TextureUniqPtr create_texture ( SDL_Renderer *renderer, const char *filename, const SDL_Color &color ) {
  auto surfacePtr = create_surface< SurfaceUniqPtr > ( filename, color );
  if ( !surfacePtr ) {
    throw SDL2Exception ( "Failed to create Texture." );
  }
  return surface_to_texture< TextureUniqPtr > ( renderer, std::move ( surfacePtr ) );
}

template <>
inline TextureShrdPtr create_texture ( SDL_Renderer *renderer, const char *filename, const SDL_Color &color ) {
  auto surfacePtr = create_surface< SurfaceUniqPtr > ( filename, color );
  if ( !surfacePtr ) {
    throw SDL2Exception ( "Failed to create Texture." );
  }
  return surface_to_texture< TextureShrdPtr > ( renderer, std::move ( surfacePtr ) );
}

/**
 *
 */

#ifdef USE_SDL2_IMAGE
template < typename T >
T create_texture ( SDL_Renderer *renderer, const char *filename, bool enableBlendMode );
template <>
inline SDL_Texture *create_texture ( SDL_Renderer *renderer, const char *filename, bool enableBlendMode ) {
  auto texturePtr = IMG_LoadTexture ( renderer, filename );
  if ( !texturePtr ) {
    throw SDL2Exception ( "Failed to create Texture." );
  }
  if ( enableBlendMode ) {
    sdl2::set_blend_mode ( texturePtr, BLENDMODE::BLEND );
  }
  return texturePtr;
}

template <>
inline TextureUniqPtr create_texture ( SDL_Renderer *renderer, const char *filename, bool enableBlendMode ) {
  return TextureUniqPtr ( create_texture< SDL_Texture * > ( renderer, filename, enableBlendMode ) );
}

template <>
inline TextureShrdPtr create_texture ( SDL_Renderer *renderer, const char *filename, bool enableBlendMode ) {
  return TextureShrdPtr ( create_texture< TextureUniqPtr > ( renderer, filename, enableBlendMode ) );
}

#endif

// template<typename T>
// decltype(auto) create_texture(SDL_Renderer *renderer,const char *filename,
// bool enableBlendMode) {}

/**
 *
 **/
inline auto get_texture_dimension ( SDL_Texture *ptr ) {
  int w, h = 0;
  if ( SDL_QueryTexture ( ptr, nullptr, nullptr, &w, &h ) != 0 ) {
    throw SDL2Exception ( "Failed to query Texture." );
  }
  return std::make_tuple ( w, h );
}
/**
 *
 **/
inline void set_blend_mode ( SDL_Texture *texture, BLENDMODE mode = BLENDMODE::NONE ) {
  SDL_BlendMode blendmode = static_cast< SDL_BlendMode > ( internal::to_type ( mode ) );
  if ( SDL_SetTextureBlendMode ( texture, blendmode ) != 0 ) {
    throw SDL2Exception ( "Failed to set Texture Blend mode state." );
  }
}

inline BLENDMODE get_blend_mode ( SDL_Texture *texture ) {
  SDL_BlendMode blendmode = SDL_BLENDMODE_NONE;
  if ( SDL_GetTextureBlendMode ( texture, &blendmode ) != 0 ) {
    throw SDL2Exception ( "Failed to get Texture Blend mode state." );
  }
  return static_cast< BLENDMODE > ( blendmode );
}
/**
 *
 **/

inline std::uint8_t get_texture_alpha ( SDL_Texture *texture ) {
  std::uint8_t alpha;
  SDL_GetTextureAlphaMod ( texture, &alpha );
  return alpha;
}
inline void set_texture_alpha ( SDL_Texture *texture, int alpha = 255 ) { SDL_SetTextureAlphaMod ( texture, alpha ); }

/**
 *
 **/
inline void set_transparent_pixel ( SDL_Surface *ptr, bool enable, int r = 0, int g = 0, int b = 0 ) {
  if ( ptr ) {
    if ( SDL_SetColorKey ( ptr, enable ? SDL_TRUE : SDL_FALSE, SDL_MapRGB ( ptr->format, r, g, b ) ) != 0 ) {
      throw SDL2Exception ( "Failed to set color key" );
    }
  }
}

#ifdef USE_SDL2_TTF
/**
 *
 **/
template < typename T >
T create_ttf ( const char *filename, int pixelHeight, int index );

template <>
inline TTF_Font *create_ttf ( const char *filename, int pixelHeight, int index ) {
  return TTF_OpenFontIndex ( filename, pixelHeight, index );
}

template <>
inline TTFUniqPtr create_ttf ( const char *filename, int pixelHeight, int index ) {
  return TTFUniqPtr ( create_ttf< TTF_Font * > ( filename, pixelHeight, index ) );
}

template <>
inline TTFShrdPtr create_ttf< TTFShrdPtr > ( const char *filename, int pixelHeight, int index ) {
  return TTFShrdPtr ( create_ttf< TTFUniqPtr > ( filename, pixelHeight, index ) );
}
#endif

#ifdef USE_SDL2_MIXER

template < typename T >
inline T create_chunk ( std::string filename );

template <>
inline Mix_Chunk *create_chunk ( std::string filename ) {
  return Mix_LoadWAV ( filename.c_str ( ) );
}
template <>
inline MixChunkUniqPtr create_chunk ( std::string filename ) {
  return MixChunkUniqPtr ( create_chunk< Mix_Chunk * > ( filename ) );
}
template <>
inline MixChunkShrdPtr create_chunk ( std::string filename ) {
  return MixChunkShrdPtr ( create_chunk< MixChunkUniqPtr > ( filename ) );
}

template < typename T >
inline T create_music ( std::string filename );

template <>
inline Mix_Music *create_music ( std::string filename ) {
  return Mix_LoadMUS ( filename.c_str ( ) );
}
template <>
inline MixMusicUniqPtr create_music ( std::string filename ) {
  return MixMusicUniqPtr ( create_music< Mix_Music * > ( filename ) );
}
template <>
inline MixMusicShrdPtr create_music ( std::string filename ) {
  return MixMusicShrdPtr ( create_music< MixMusicUniqPtr > ( filename ) );
}

inline int play_chunk ( Mix_Chunk *chunk, int channel = -1, int loop = 0 ) {
  int rtn = Mix_PlayChannel ( channel, chunk, loop );
  if ( -1 == rtn ) {
    // TODO: throw exception
    printf ( "ERROR PLAYING\n" );
  }
  return rtn;
}
inline bool is_channel_playing ( int channel ) { return Mix_Playing ( channel ) != 0; }
inline int get_channels_playing ( ) { return Mix_Playing ( -1 ); }
#endif

#ifdef USE_GL
/**
 *
 **/
template < typename T >
T create_gl_context ( SDL_Window *window );

template <>
inline SDL_GLContext create_gl_context ( SDL_Window *window ) {
  if ( !window ) {
    return nullptr;
  }
  return SDL_GL_CreateContext ( window );
}
template <>
inline GLContextUniqPtr create_gl_context ( SDL_Window *window ) {
  if ( !window ) {
    return nullptr;
  }
  return GLContextUniqPtr ( static_cast< void ** > ( create_gl_context< SDL_GLContext > ( window ) ) );
}
inline void setVsync ( bool vsync ) { SDL_GL_SetSwapInterval ( vsync ); }
#endif

/**
 *
 **/
inline auto get_window_size ( SDL_Window *window ) {
  int width, height;
  SDL_GetWindowSize ( window, &width, &height );
  return std::make_tuple ( width, height );
}
inline auto get_window_position ( SDL_Window *window ) {
  int width, height;
  SDL_GetWindowPosition ( window, &width, &height );
  return std::make_tuple ( width, height );
}
inline auto get_window_aspect_ratio ( SDL_Window *window ) {
  auto size = get_window_size ( window );
  return std::get< 0 > ( size ) / std::get< 1 > ( size );
}

inline void set_window_title ( SDL_Window *window, const char *text ) { SDL_SetWindowTitle ( window, text ); }
/**
 *
 **/
inline void set_renderer ( SDL_Renderer *renderer, SDL_Texture *texture ) { SDL_SetRenderTarget ( renderer, texture ); }
/**
 *
 **/
inline void clear ( SDL_Renderer *renderer, SDL_Color color = {0, 0, 0, 0} ) {
  SDL_SetRenderDrawColor ( renderer, color.r, color.g, color.b, color.a );
  SDL_RenderClear ( renderer );
}
/**
 *
 **/
inline auto get_viewport ( SDL_Renderer *renderer ) {
  SDL_Rect rect;
  SDL_RenderGetViewport ( renderer, &rect );
  return std::make_tuple ( rect.w, rect.h, rect.x, rect.y );
}
inline void set_viewport ( SDL_Renderer *renderer, int width, int height, int x, int y ) {
  SDL_Rect viewPort = {width, height, x, y};
  if ( SDL_RenderSetViewport ( renderer, &viewPort ) ) {
    throw SDL2Exception ( "Failed to set viewport." );
  }
}
/**
 *
 **/
inline auto get_scale ( SDL_Renderer *renderer ) {
  float scaleX, scaleY;
  SDL_RenderGetScale ( renderer, &scaleX, &scaleY );
  return std::make_tuple ( scaleX, scaleY );
}

inline void set_scale ( SDL_Renderer *renderer, float x, float y ) {
  if ( SDL_RenderSetScale ( renderer, x, y ) != 0 ) {
    throw SDL2Exception ( "Failed to set scale." );
  }
}
/**
 *
 **/
inline auto get_logical_size ( SDL_Renderer *renderer ) {
  int width, height;
  SDL_RenderGetLogicalSize ( renderer, &width, &height );
  return std::make_tuple ( width, height );
}
inline void set_logical_size ( SDL_Renderer *renderer, int width, int height ) {
  if ( SDL_RenderSetLogicalSize ( renderer, width, height ) != 0 ) {
    throw SDL2Exception ( "Failed to set logical size." );
  }
}
/**
 *
 **/
inline void swap ( SDL_Window *window ) { SDL_GL_SwapWindow ( window ); }
inline void swap ( SDL_Renderer *renderer ) { SDL_RenderPresent ( renderer ); }
/**
 *
 **/
inline void render_texture ( SDL_Renderer *renderer, SDL_Texture *texture, int x1, int y1, int x2, int y2 ) {
  int w, h;

  SDL_QueryTexture ( texture, nullptr, nullptr, &w, &h );
  const SDL_Rect src = {x1, y1, w, h};
  const SDL_Rect dst = {x2, y2, w, h};

  if ( SDL_RenderCopy ( renderer, texture, &src, &dst ) ) {
    throw SDL2Exception ( "Failed to render texture." );
  }
}
/**
 *
 **/
inline void render_texture_at ( SDL_Renderer *renderer, SDL_Texture *texture, const SDL_Rect *clip, int x, int y ) {
  SDL_Rect dst = {x, y, 0, 0};
  if ( clip ) {
    dst.w = clip->w;
    dst.h = clip->h;
  } else {
    int w, h;
    SDL_QueryTexture ( texture, nullptr, nullptr, &w, &h );
    dst.w = w;
    dst.h = h;
  }

  if ( SDL_RenderCopy ( renderer, texture, clip, &dst ) ) {
    throw SDL2Exception ( "Failed to render texture." );
  }
}
/**
 *
 **/
inline void render_rect ( SDL_Renderer *renderer, const SDL_Rect &rec, const SDL_Color &color ) {
  SDL_SetRenderDrawColor ( renderer, color.r, color.g, color.b, color.a );
  SDL_RenderFillRect ( renderer, &rec );
}

inline void render_rects ( SDL_Renderer *renderer, const SDL_Rect *rects, std::size_t size ) {
  if ( SDL_RenderDrawRects ( renderer, rects, size ) != 0 ) {
    throw SDL2Exception ( "Failed to create Texture." );
  }
}
struct RectBatch {
  void append ( std::initializer_list< SDL_Rect > rec ) { vec.insert ( vec.end ( ), rec.begin ( ), rec.end ( ) ); }
  void append ( SDL_Rect rec ) { vec.emplace_back ( std::move ( rec ) ); }
  std::pair< const SDL_Rect *, std::size_t > c_arr ( ) const { return {&vec[ 0 ], vec.size ( )}; }
  void renderRects ( SDL_Renderer *renderer, SDL_Color color ) {
    SDL_SetRenderDrawColor ( renderer, color.r, color.g, color.b, color.a );
    SDL_RenderFillRects ( renderer, &vec[ 0 ], vec.size ( ) );
  }

 private:
  std::vector< SDL_Rect > vec;
};

// TODO: figure out how to use partial specialization using more than one
// template argument and different return types.
// http://stackoverflow.com/questions/5101516/why-function-template-cannot-be-partially-specialized

template < typename FUNC >
inline SDL_Texture *render_to_texture_raw ( FUNC &&func, SDL_Renderer *renderer, int texturewidth, int textureheight ) {
  auto texLayer = create_texture< SDL_Texture * > ( renderer, SDL_PIXELFORMAT_ARGB8888, SDL_TEXTUREACCESS_TARGET,
                                                    texturewidth, textureheight, true );
  set_renderer ( renderer, texLayer );
  clear ( renderer );
  func ( );
  set_renderer ( renderer, nullptr );
  return texLayer;
}

template < typename FUNC >
inline decltype ( auto ) render_to_texture_uniq ( FUNC &&func, SDL_Renderer *renderer, int texturewidth,
                                                  int textureheight ) {
  return TextureUniqPtr (
      render_to_texture_raw ( std::forward< FUNC > ( func ), renderer, texturewidth, textureheight ) );
}

template < typename FUNC >
inline decltype ( auto ) render_to_texture_shrd ( FUNC &&func, SDL_Renderer *renderer, int texturewidth,
                                                  int textureheight ) {
  return TextureShrdPtr (
      render_to_texture_uniq ( std::forward< FUNC > ( func ), renderer, texturewidth, textureheight ) );
}

/**
 *
 **/
inline auto get_version ( SubSystemFlags subSystem ) {
  SDL_version version{0, 0, 0};
  switch ( subSystem ) {
    case SubSystemFlags::CORE:
      SDL_VERSION ( &version );
      break;
    case SubSystemFlags::IMAGE:
#ifdef USE_SDL2_IMAGE
      SDL_IMAGE_VERSION ( &version );
#endif
      break;
    case SubSystemFlags::TTF:
#ifdef USE_SDL2_TTF
      SDL_TTF_VERSION ( &version );
#endif
      break;
    case SubSystemFlags::AUDIO:
      // TODO: Implement
      break;
  }
  // TODO: cast me
  return std::make_tuple ( static_cast< std::uint32_t > ( version.major ),
                           static_cast< std::uint32_t > ( version.minor ),
                           static_cast< std::uint32_t > ( version.patch ) );
}
/**
 *
 **/
inline auto get_linked_version ( SubSystemFlags subSystem ) {
  SDL_version linked{0, 0, 0};
  switch ( subSystem ) {
    case SubSystemFlags::CORE:
      SDL_GetVersion ( &linked );
      break;
    case SubSystemFlags::IMAGE:
#if USE_SDL2_IMAGE
      linked = *IMG_Linked_Version ( );
#endif
      break;
    case SubSystemFlags::TTF:
#if USE_SDL2_TTF
      linked = *TTF_Linked_Version ( );
#endif
      break;
    case SubSystemFlags::AUDIO:
      break;
    default:
      break;
  }

  return std::make_tuple ( static_cast< std::uint32_t > ( linked.major ), static_cast< std::uint32_t > ( linked.minor ),
                           static_cast< std::uint32_t > ( linked.patch ) );
}
/**
 *
 **/
inline void enable_mouse_grab ( SDL_Window *window, bool enable ) {
  if ( enable && SDL_GetRelativeMouseMode ( ) == SDL_TRUE ) {
    SDL_SetRelativeMouseMode ( SDL_FALSE );
    SDL_SetWindowGrab ( window, SDL_FALSE );
  } else {
    SDL_SetRelativeMouseMode ( SDL_TRUE );
    SDL_SetWindowGrab ( window, SDL_TRUE );
  }
}

inline void enable_mouse_cursor ( bool enable ) { SDL_ShowCursor ( enable ? SDL_ENABLE : SDL_DISABLE ); }

#ifdef USE_SDL2_TTF
/**
 *
 **/
template < typename T >
T render_ttf ( TTF_Font *font, TTFRenderMode renderMode, const char *text, SDL_Color fg, SDL_Color bg = {0, 0, 0, 0} );

template <>
inline SDL_Surface *render_ttf ( TTF_Font *font, TTFRenderMode renderMode, const char *text, SDL_Color fg,
                                 SDL_Color bg ) {
  SDL_Surface *surfacePtr = nullptr;
  switch ( renderMode ) {
    case TTFRenderMode::Blended:
      surfacePtr = TTF_RenderText_Blended ( font, text, fg );
      break;
    case TTFRenderMode::Shaded:
      surfacePtr = TTF_RenderText_Shaded ( font, text, fg, bg );
      break;
    case TTFRenderMode::Solid:
      surfacePtr = TTF_RenderText_Solid ( font, text, fg );
      break;
    default:
      break;
  }
  return surfacePtr;
}
template <>
inline SurfaceUniqPtr render_ttf ( TTF_Font *font, TTFRenderMode renderMode, const char *text, SDL_Color fg,
                                   SDL_Color bg ) {
  return SurfaceUniqPtr ( render_ttf< SDL_Surface * > ( font, renderMode, text, fg, bg ) );
}

template <>
inline SurfaceShrdPtr render_ttf ( TTF_Font *font, TTFRenderMode renderMode, const char *text, SDL_Color fg,
                                   SDL_Color bg ) {
  return SurfaceShrdPtr ( render_ttf< SurfaceUniqPtr > ( font, renderMode, text, fg, bg ) );
}

#endif

#ifdef USE_GL
struct GL_Attributes {
  int major{3};
  int minor{3};
  int depthSize{24};
  SDL_Color colorSize{8, 8, 8, 8};
  bool enableDoubleBuffer{true};
};
/**
 * Sets the core profile and other gl attributes
 * https://wiki.libsdl.org/SDL_GL_SetAttribute
 */
inline void init_gl_attribute ( const GL_Attributes &attributes = GL_Attributes ( ) ) {
#ifdef USE_GLES2
  SDL_GL_SetAttribute ( SDL_GL_CONTEXT_MAJOR_VERSION, 2 );
  SDL_GL_SetAttribute ( SDL_GL_CONTEXT_MINOR_VERSION, 0 );
  SDL_GL_SetAttribute ( SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_ES );
#else
  SDL_GL_SetAttribute ( SDL_GL_CONTEXT_MAJOR_VERSION, attributes.major );
  SDL_GL_SetAttribute ( SDL_GL_CONTEXT_MINOR_VERSION, attributes.minor );
  SDL_GL_SetAttribute ( SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE );
#endif
  //
  SDL_GL_SetAttribute ( SDL_GL_RED_SIZE, attributes.colorSize.r );
  SDL_GL_SetAttribute ( SDL_GL_GREEN_SIZE, attributes.colorSize.g );
  SDL_GL_SetAttribute ( SDL_GL_BLUE_SIZE, attributes.colorSize.b );
  SDL_GL_SetAttribute ( SDL_GL_ALPHA_SIZE, attributes.colorSize.a );

  // Enables double buffering
  SDL_GL_SetAttribute ( SDL_GL_DOUBLEBUFFER, attributes.enableDoubleBuffer );
  // 24 bit depth buffer
  SDL_GL_SetAttribute ( SDL_GL_DEPTH_SIZE, attributes.depthSize );
}

#endif

}  // end of namespace sdl2
