#pragma once
#include "rgd/RGD_sdl2.hpp"

class Menu final {
 public:
  Menu ( );
  ~Menu ( ) = default;

 public:
  enum Selection : int {
    //
    BEGIN = 0,
    //
    PLAYER_ONE,
    //
    PLAYER_TWO,
    //
    QUIT,
    //
    END
  };

 public:
  struct MenuFont {
    sdl2::TextureShrdPtr backgroundFontTexture;
    sdl2::TextureShrdPtr foregroundFontTexture;
    Selection menuItem;
  };

 public:
  Menu::Selection getCurrentMenuItem ( ) { return currentMenuItem_; }
  void next ( );
  void prev ( );
  void render ( );
  //  void update ( double delta );

 private:
  Selection currentMenuItem_{Selection::PLAYER_ONE};
  MenuFont menuFont[ Selection::END + 1 ];
};
