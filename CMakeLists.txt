cmake_minimum_required(VERSION 3.3)
project(dotsnboxes)

add_definitions(-DUSE_SDL2_TTF)

option(BUILD_TEST "Builds all unit tests" OFF)
option(USE_EMSCRIPTEN "Uses Emscripten" OFF)
#
# If using qt creator, this will display the header files in the project
#
FILE(GLOB_RECURSE ${PROJECT_HOME}-headerfiles "include/*.hpp" )
add_custom_target(headers SOURCES ${${PROJECT_HOME}-headerfiles})


if(USE_EMSCRIPTEN)
  if (NOT "$ENV{EMSCRIPTEN}" STREQUAL "")
    set(CMAKE_C_COMPILER $ENV{EMSCRIPTEN}/emcc)
    set(CMAKE_CXX_COMPILER  $ENV{EMSCRIPTEN}/em++)
    set(CMAKE_AR  $ENV{EMSCRIPTEN}/emar)
    set(CMAKE_RANLIB $ENV{EMSCRIPTEN}/emranlib)
  endif()
  find_program(EMPP_CHECK_COMMAND em++)
  if(NOT EMPP_CHECK_COMMAND)
    message(FATAL_ERROR "Em++ Not found, Make sure to source in the 'source ./emsdk_env.sh' ")
  endif()
  set(CMAKE_CXX_COMPILER_ID "Emscripten")
endif()

set (CMAKE_CXX_STANDARD 14)
if (${CMAKE_CXX_COMPILER_ID} STREQUAL "GNU" OR ${CMAKE_CXX_COMPILER_ID} STREQUAL "Clang")
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra -g")
  set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS} ${CMAKE_CXX_FLAGS_DEBUG} -g")
  set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS} ${CMAKE_CXX_FLAGS_RELEASE} -O2")
elseif(${CMAKE_CXX_COMPILER_ID} STREQUAL "Emscripten")
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra -ffast-math")
  #Note: we still need to specify the -std=c++14 flag
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14 -s USE_SDL=2 -s USE_SDL_TTF=2 ")
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} --preload-file assets --use-preload-plugins")
else()
  message(FATAL_ERROR "Unable to detect Compiler")
endif()
#
# Find sources/includes
#
add_subdirectory("externals/")
include_directories("${PROJECT_SOURCE_DIR}/include/")
aux_source_directory("${PROJECT_SOURCE_DIR}/source/" ${PROJECT_NAME}_SRC)
#
# Build binary
#
if(USE_EMSCRIPTEN)
  set(${PROJECT_NAME}_WEB "${PROJECT_NAME}.html")
  add_custom_target(web DEPENDS ${${PROJECT_NAME}_WEB} copy-data-files)
  add_executable(${${PROJECT_NAME}_WEB} main.cpp ${${PROJECT_NAME}_SRC})
else()
  find_package(PkgConfig REQUIRED)
  foreach(requiredDependencies zlib sdl2 SDL2_mixer SDL2_ttf SDL2_gfx)
    pkg_search_module(${requiredDependencies} REQUIRED ${requiredDependencies})
    list(APPEND REQUIRED_LIBRARIES ${${requiredDependencies}_LIBRARIES})
  endforeach()
  add_executable(${PROJECT_NAME} main.cpp ${${PROJECT_NAME}_SRC})
  target_link_libraries(${PROJECT_NAME} 
    ${REQUIRED_LIBRARIES}
    )
endif()
file(COPY assets DESTINATION ./)
#
# Build test if enabled
#
if(BUILD_TEST)
  message(STATUS "Enabling testing for ${PROJECT_NAME}")
  enable_testing()
  add_subdirectory("test/")
endif()
