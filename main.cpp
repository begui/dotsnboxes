#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include "circle.hpp"
#include "game.hpp"
#include "rgd/RGD_sdl2.hpp"

constexpr auto WIDTH{800};
constexpr auto HEIGHT{600};

std::string font_item{"./assets/Roboto-Regular.ttf"};

int main ( int argc, char* argv[] ) {
  //
  // Init sdl
  //
  sdl2::init ( sdl2::INIT::VIDEO | sdl2::INIT::EVENTS | sdl2::INIT::TIMER );
  sdl2::init_ttf ( );
  std::atexit ( TTF_Quit );
  std::atexit ( SDL_Quit );
  //
  // Print library version
  //
  auto version = sdl2::get_version ( sdl2::SubSystemFlags::CORE );
  auto linkedversion = sdl2::get_linked_version ( sdl2::SubSystemFlags::CORE );
  std::printf ( "SDL2 %d.%d.%d Revision %d.%d.%d\n", std::get< 0 > ( version ), std::get< 1 > ( version ),
                std::get< 2 > ( version ), std::get< 0 > ( linkedversion ), std::get< 1 > ( linkedversion ),
                std::get< 2 > ( linkedversion ) );

  version = sdl2::get_version ( sdl2::SubSystemFlags::TTF );
  linkedversion = sdl2::get_linked_version ( sdl2::SubSystemFlags::TTF );
  std::printf ( "SDL2 TTF %d.%d.%d Revision %d.%d.%d\n", std::get< 0 > ( version ), std::get< 1 > ( version ),
                std::get< 2 > ( version ), std::get< 0 > ( linkedversion ), std::get< 1 > ( linkedversion ),
                std::get< 2 > ( linkedversion ) );
  //
  // Create window
  //
  auto windowFlags{0x00};
  sdl2::WindowUniqPtr window = sdl2::create_window< decltype ( window ) > ( windowFlags, WIDTH, HEIGHT );
  sdl2::set_window_title ( window.get ( ), "Minimax" );
  //
  // Create renderer
  //
  auto renderFlags{0x00};
  renderFlags |= sdl2::RENDERER::ACCELERATED;
  renderFlags |= sdl2::RENDERER::VSYNC;
  sdl2::RendererUniqPtr renderer = sdl2::create_renderer< decltype ( renderer ) > ( window.get ( ), renderFlags );
  sdl2::set_logical_size ( renderer.get ( ), 800, 600 );
  //
  // Create font
  //
  auto font = sdl2::create_ttf< sdl2::TTFShrdPtr > ( font_item.c_str ( ), 24, 0 );
  if ( font == nullptr ) {
    throw sdl2::SDL2Exception( "Failed to load font Error: " + std::string ( TTF_GetError ( ) ) );
  }
  //
  // Start the game
  //
  Game game;
  game.setWindow ( window.get ( ) );
  game.setRenderer ( renderer.get ( ) );
  game.setFont ( font.get ( ) );

  game.run ( );

  return EXIT_SUCCESS;
}
